# **Nepal Investment Bank BackEnd**

## **Pre-requisites**
- IIS (Internet Information Services)
- Any suitable GIT client
- Notepad/Notepad++

*This guide is for deploying and updating the app in local environment*

## **Deploy**
1.	Clone the repository with any suitable GIT client into the folder where app is to be deployed.
2.	Alternatively, you can download the repository as a ZIP file.
3.	Host the app in IIS.

## **Database Configuration in web.config**
1.	Navigate to the folder where app to be deployed.
2.	Navigate to the file web.config within the root path and edit in notepad.
3.	Find the key name NIBLDbContext between the tags <connectionstrings></connectionstrings> and edit the following with production database information.

- Server	:	Server name of database instance (domain or ip address)
- Uid		:	Username
- Pwd		:	Password
- Database	:	Database name

*For example:* 

- Server	=	127.0.0.1
- Uid		=	nibl_user
- Pwd		=	nibl_password
- Database	=	db_nibl
4.	Save the file.

## **Change Domain Url Path Configuration in web.config**
- Find the following key : <add key="NIBLDomainName" value="http://kushal123-001-site1.ctempurl.com" />
- Replace it with your domain : <add key="NIBLDomainName" value="http://domain-name.com" />
- For Example : <add key="NIBLDomainName" value="http://localhost:8080" />